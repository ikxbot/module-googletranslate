<?php
namespace Ikx\GoogleTranslate\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\MessagingTrait;
use \Statickidz\GoogleTranslate;

class Translate extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    /**
     * Command runner
     */
    public function run()
    {
        if (count($this->params) >= 3) {
            $source = $this->params[0];
            $target = $this->params[1];
            $text = $this->params;
            array_shift($text);
            array_shift($text);

            $trans = new GoogleTranslate();
            $result = $trans->translate($source, $target, implode(' ', $text));
            $this->msg($this->channel, $result);
        } else {
            $this->msg($this->channel, __('Syntax: %s <source> <target> <text>', $this->network->get('prefix') . $this->command));
        }
    }

    /**
     * Command describer (used for help)
     */
    public function describe()
    {
        return "Translate a string with Google Translate";
    }
}